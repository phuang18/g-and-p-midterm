/**                                                                                                     
Names: Peter Huang and Graham Reynolds                                                                  
JHEDs: phuang18 and jreyno37                                                                            
Emails: phuang18@jhu.edu and jreyno37@jhu.edu                                                           
Date: 10/7/2017                                                                                         
Course: 601.220                                                                                         
Assignment: Midterm Project
*/

/**
 Header file to contain functions for ppmIO.c
*/

#ifndef PPMIO_H
#define PPMIO_H
#include <stdlib.h>
#include <stdio.h>

/**
 Defintion of a pixel structure
*/
typedef struct pixel{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
} pix;

/**
 Definition of an image structure
*/
typedef struct image{
  pix *data;
  int rows;
  int cols;
  int colors;
} img;

/**
 Definition of a pixl structure with double rgb values
*/
typedef struct double_pixel{
  double red;
  double green;
  double blue;
} dpix;

/**
 Function that writes a P6 image to a file
*/
int write(img *base, const char *file); 

/**
 Function that writes a P6 image to a file with components of an image
*/
int writePPM(pix *image, int rows, int cols, int colors, FILE *fp);

/**
 Function that writes a P6 image to a file with pointer to an image
*/
int writePPMImage(img *im, FILE *fp);

/**
 Function that writes a P6 image to a file given a filenae
*/
int writePPMImageFile(img *im, char *filename);	

/**
 Function athat 
*/
int readPPM(const char *filename, img **image);

#endif
