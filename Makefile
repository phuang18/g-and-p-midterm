CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra

# Links together files needed to create executable
comp: main.o menu.o ppmIO.o imageManip.o
	$(CC) -lm -g -o comp main.o menu.o ppmIO.o imageManip.o

# Compiles main.c to create main.o
main.o: main.c menu.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c main.c

# Compiles menu.c to create menu.o
menu.o: menu.c menu.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c menu.c

# Compiles ppmIO.c to creat ppmIO.o
ppmIO.o: ppmIO.c ppmIO.h menu.h imageManip.h
	$(CC) $(CFLAGS) -c ppmIO.c

# Compiles imageManip.c to create imageManip.o
imageManip.o: imageManip.c menu.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

# Removes all object files and the executable named comp,
clean:
	rm -f *.o comp
