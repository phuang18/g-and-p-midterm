/**
Names: Peter Huang and Graham Reynolds
JHEDS: phuang18 and jreyno37
Email: phuang18@jhu.edu and jyreno37@jhu.edu
Date: 10/7/2017
Course Number: 601.220
Assignment: Midterm Project
*/

/**
 Header file to have functions for image manipulation
*/

#ifndef IMAGE_MANIP_H
#define IMAGE_MANIP_H
#define pi 3.1415926
#include "ppmIO.h"
#include <math.h>

/**
 * Function that takes 4 points as corners and returns a cropped image
 */
void crop(img **data_pointer, int left, int right, int top, int bottom);


/**
 * Function that sets takes an double value and a change value and clamps them to a range [0,255]
 */
unsigned char saturation (double intial , int change);

/**
 * Function that adjusts the brightness of an image
 */
void brightness (img **data_pointer, int change);

/**
 Function that converts an image into grayscale
 */
void grayscale(img **data_pointer);

/**
 Function that swaps the color values for an image
 */
void swap(img **data_pointer);

/**
 Function that takes a sigma value and returns the value * 5, + 1 to make if odd if necessary
 */
int transform_sigma(double sigma);

/**
 Function that returns the square of a value
 */
double sq(double value);

/**
 Function that fills a matrix with weights based on their distance from the center 
 */
void populate_matrix(int transformed_sigma, double weights[][transformed_sigma], double sigma);

/**
 Sets the lower limit for a 2D array (both top and left)
 */
int set_l_t(int pix_pos, double radius);

/**
 Sets the upper limit for a 2D array (both bottom and right)
 */
int set_r_b(int pix_pos, double radius, int max);

/**
 Adds the values in a matrix and reports it as a double
 */
double matrix_into_char(int size, double matrix[size][size], int top, int bot, int right, int left);

/**
 Takes the pieces required to build and img structure and a pointer and sets the pointer to the image
 */
void pieces_to_pointer(img **data_pointer, pix *structure, int columns, int rows, int colors);

/**
 Blurs an image by a certain amount
 */
void blur(int transformed_sigma, double sigma, img **data_pointer);

/**
 Sharpens the image by a certain amount
 */
void sharpen(img **data_pointer, double sigma, double amount);

/**
 Function that produces contrast on an image
 */
void contrast(img **data_pointer, double adjustment);

/**
 Converts a double back into an unsigned char
 */
unsigned char convert_back(double dub);

/**
 Converts an unsigned char into a double
 */
double convert_double(unsigned char uc);

/**
 Function that detects edges in an image
 */
void edge_detection(img **data_pointer, unsigned char threshold, int transformed_sigma, double sigma);

/**
 Function that computes gradient of x component
 */
unsigned char gradIx(img **data_pointer, int row, int col);

/**
 Function that computes gradient of y component
 */
unsigned char gradIy(img **data_pointer, int row, int col);

/**
 Function that computes gradient
*/
unsigned char gradMag(unsigned char gradIx, unsigned char gradIy);

#endif
