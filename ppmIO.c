/**                                                                                                     
Names: Peter Huang and Graham Reynolds                                                                  
JHEDs: phuang18 and jreyno37                                                                            
Emails: phuang18@jhu.edu and jreyno37@jhu.edu                                                           
Date: 10/7/2017                                                                                         
Course: 601.220                                                                                         
Assignment: Midterm Project                                                                             
*/

/**
 Contains functions to work with images
*/

#include "ppmIO.h"
#include <stdlib.h>
#include <stdio.h>

//the function to read the PPM image file in
int readPPM(const char *filename, img **image){
	
	char word[255];
	FILE *fp;
	
	//file pointer to recieve the user input file
	fp = fopen(filename, "rb");
	
	//make sure a file name was input
	if (fp == NULL) {
		fprintf(stderr, "Error opening file %s.\n", filename);
		fclose(fp);
		return 1;
	}
	
	//read in the first line
	fgets(word, sizeof(word), fp);
	
	//the first line should be P6, else terminate
	if (word[0] != 'P' || word[1] != '6'){
		fprintf(stderr, "Error opening file %s.\n", filename);
		fclose(fp);
		return 1;
	}
	
	//allocate enough space for the image in memory
        *image = malloc(sizeof(img));
       
	
	//get to the next line
	while (fgetc(fp) != '\n') ;

	//the next 2 values should be number of columns and rows
	if (fscanf(fp, "%d %d ", &(*image)->cols, &(*image)->rows) != 2) {
		fprintf(stderr, "Error opening file %s.\n", filename);
                fclose(fp);
		return 1;
	}
	
	//the next value is the color value
	if (fscanf(fp, "%d", &(*image)->colors) != 1){
		fprintf(stderr, "Error opening file %s.\n", filename);
                fclose(fp);
		return 1;
	}
	
	//the number of color shades must be equal to 255
	if ((*image)->colors != 255){
		fprintf(stderr, "Error opening file %s.\n", filename);
                fclose(fp);
		return 1;
	}

	//get to the next line
        while (fgetc(fp) != '\n') ;

	//create data array of pix structs
	(*image)->data = malloc((*image)->rows * (*image)->cols * sizeof(pix));

	//read in all values to the pix data array
	if(fread((*image)->data, sizeof(pix), ((*image)->cols * (*image)->rows), fp) != (unsigned int)( (*image)->rows * (*image)->cols )  )  {
	        fprintf(stderr, "Error opening file %s.\n", filename);
		fclose(fp);
		return 1;
	}
	//close the file pointer
	fclose(fp);
	return 0;
}

int write(img *base, const char *file){

  // define dimensions and color for our image
  int rows = base->rows;
  //printf("%d\n", rows);
  int cols = base->cols;
  //printf("%d\n", cols);
 
  // allocate space for pixels                                     
  pix *pixel = malloc(sizeof(pix) * rows * cols);
  
  // fill image (sets each pixels to the same color)           
  for (int r = 0; r < rows; ++r) {
   for (int c = 0; c < cols; ++c) {
      pix color = base->data[(r * cols) + c];
      pixel[(r * cols) + c] = color;
   }
 }

  // write image to disk
  FILE *fp = fopen(file, "wb"); //open binary file for writing
  writePPM(pixel, rows, cols, 255, fp);
  fclose(fp);

  // encapsulate in an Image struct         
  img im;
  im.data = pixel;
  im.rows = rows;
  im.cols = cols;
  im.colors = 255;
  
  // use wrapper function to write the Image struct
  fp = fopen(file, "wb");
  writePPMImage(&im, fp);
  fclose(fp);

  // free the memory that we allocated!
  // We could also have used:      free(pix);  
  free(im.data);

  return 0;
}

int writePPM(pix *image, int rows, int cols, int colors, FILE *fp){
  if (!fp){
    fprintf(stderr, "Error\n");
    return 0;
  }

  fprintf(fp, "P6\n");
  fprintf(fp, "%d %d\n%d\n", cols, rows, colors);
  
  fwrite(image, sizeof(pix), rows * cols, fp);
  int written = fwrite(image, sizeof(pix), rows * cols, fp);
  if (written != (rows*cols)) {
     fprintf(stderr, "Error\n");
  }
  return written;
}

int writePPMImage(img *im, FILE *fp){
  return writePPM(im->data, im->rows, im->cols, im->colors, fp);
}


int writePPMImageFile(img *im, char *filename){
  FILE *fp = fopen(filename, "wb");

  if (!fp) {
    fprintf(stderr, "Error\n");
    return 0;
  }
  int written = writePPM(im->data, im->rows, im->cols, im->colors, fp);
  write(im, filename);
  fclose(fp);
  return written;
}
  

