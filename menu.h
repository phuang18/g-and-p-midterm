/**                                                                                                     
Names: Peter Huang and Graham Reynolds                                                                  
JHEDs: phuang18 and jreyno37                                                                            
Emails: phuang18@jhu.edu and jreyno37@jhu.edu                                                           
Date: 10/7/2017                                                                                         
Course: 601.220                                                                                         
Assignment: Midterm Project                                                                                                                                               
*/

/**
 Header file to hold functions for menu.c
*/

#ifndef MENU_H
#define MENU_H
#include <stdlib.h>
#include <stdio.h>

/**
 Function that calls the menu swtich to run specific cases
 */
void menu();

/**
 Function that ouputs menu options
*/
void print_menu();


#endif
