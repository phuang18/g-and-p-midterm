/**                                                                                                     
Names: Peter Huang and Graham Reynolds                                                                  
JHEDs: phuang18 and jreyno37                                                                            
Emails: phuang18@jhu.edu and jreyno37@jhu.edu                                                           
Date: 10/7/2017                                                                                         
Course: 601.220                                                                                         
Assignment: Midterm Project                                                                             */

/**
 Function to operate the menu aspect of the program
*/

#include "imageManip.h"
#include "ppmIO.h"
#include "menu.h"
#include <stdio.h>
#include <string.h>


void menu(){

  // Initalize variables to be scanned in later
  char input[4];
  char filepath[255];
  double sigma, amount, changed_sigma;
  int change, x1, x2, y1, y2;
  unsigned char threshold;

  //Initialize pointers for our img struct  
  img *data = NULL;
  img **data_pointer = &data;

  //Switch statement to control/take in user input
  do{
     print_menu();
     scanf("%s", input);
     switch(input[0]){
        case 'r':	     
	   scanf("%s", filepath);
           readPPM(filepath, &data);
	   printf("Reading %s...\n", filepath);
           break;
	   
        case 'w':
	  if(data == NULL){
	    break;
	  }
	  scanf("%s", filepath);
	  writePPMImageFile(*data_pointer, filepath);
	  printf("Writing to %s...\n", filepath);
	  break;
	  
       case 's':
	 switch(input[1]){
	    case 'h':
	      if(data == NULL){
		break;
	      }
	      scanf("%lf", &sigma);
	      scanf("%lf", &amount);
	      sharpen(data_pointer, sigma,  amount);
	      printf("Applying sharpen filter, sigma %f, intensity %f...\n", sigma, amount);
	      break;
	      
	    default:
	      if(data == NULL){
		break;
	      }
	      swap(data_pointer);
	      printf("Swapping color channels...\n");
	      break;
	 }
	 break;
	   
       case 'b':
	 switch(input[1]){
	    case 'r':
	      if(data == NULL){
		break;
	      }
	      scanf("%d", &change);
	      brightness(data_pointer, change);
	      printf("Adjusting the brightness by %d...\n", change);
	      break;
		
	    case 'l':
	      if(data == NULL){
		break;
	      }
	      scanf("%lf", &sigma);
	      changed_sigma = transform_sigma(sigma);
	      blur(changed_sigma, sigma, data_pointer);
	      printf("Applying blur filter, sigma %f...\n", sigma);
	      break;
	 }
	 break;
	 
       case 'c':
	 switch(input[1]){
	    case 'n':
	      if(data == NULL){
		break;
	      }
	      scanf("%lf", &amount);
              contrast(data_pointer, amount);
	      printf("Adjusting contrast by %f...\n", amount);
	      break;
	      
	    default:
	      if(data == NULL){
		break;
	      }
	      scanf("%d", &x1);
	      scanf("%d", &y1);
	      scanf("%d", &x2);
	      scanf("%d", &y2);
	      crop(data_pointer, x1, y1, x2, y2);
	      printf("Cropping region from (%d, %d) to (%d, %d)...\n", x1, y1, x2, y2);
	      break;
	       
	 }
	 break;
	      
       case 'g':
	 if(data == NULL){
	   break;
	 }
	 grayscale(data_pointer);
	 printf("Converting to  grayscale ...\n");
	 break;
	      
       case 'e':
	 if(data == NULL){
	   break;
	 }
	 scanf("%lf", &sigma);
	 scanf("%hhu", &threshold);
	 changed_sigma = transform_sigma(sigma);
         edge_detection(data_pointer, threshold, changed_sigma, sigma);	 
         printf("Doing edge detection with sigma %f and threshold %u...\n", sigma, threshold);
	 break;
	 
       case 'q':
	 printf("Goodbye!\n");
	 break;
	 
       default:
	 printf("Invalid input.\n");
	 break;
	 
     }
  }while(input[0] != 'q');

  //Free our image struct memory, accounting for instances where user instantly quits
  if(data != NULL){
     //free((*data_pointer)->data);
     //free(*data_pointer);
  }
}

void print_menu(){
  printf("%s", "Main menu:\n");
  printf("\t%s", "r <filename> - read image from <filename>\n");
  printf("\t%s", "w <filename> - write image to <filename>\n");
  printf("\t%s", "s - swap color channels\n");
  printf("\t%s", "br <amt> - change brightness (up or down) by the given amount\n");
  printf("\t%s", "c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
  printf("\t%s", "g - convert to grayscale\n");
  printf("\t%s", "cn <amt> - change contrast (up or down) by the given amount\n");
  printf("\t%s", "bl <sigma> - Gaussian blur with the given radius (sigma)\n");
  printf("\t%s", "sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n");
  printf("\t%s", "e <sigma> <threshold> - detect edges with intensity gradient above given threshold\n");
  printf("\t%s", "q - quit\n");
  printf("Enter choice: ");
}

  
  
  


   
