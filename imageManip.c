/**
Names: Peter Huang and Graham Reynolds
JHEDs: phuang18 and jreyno37
Emails: phuang18@jhu.edu and jreyno37@jhu.edu
Date: 10/7/2017
Course: 601.220
Assignment: Midterm Project

*/

/**
 Contains functions relating to image manipulation
*/


#include "imageManip.h"
#include "ppmIO.h"
#include <math.h>

void crop(img **data_pointer, int left, int top, int right, int bottom){
    //Set aside some dynamic memory for out cropped image
    int width = right - left;
    int length = bottom - top;
    int colors = (*data_pointer)->colors;
    pix *cropped = malloc(sizeof(pix) * width * length);
	
    for (int r = 0; r < length; r++){
      for (int c = 0; c < width; c++){

	//Shift in our base image to the correct location
	int start_row = left + r ;
        int start_column = top + c;
	int full_length = ((*data_pointer))->cols;

	//Extract the color values for each specific pixel
	cropped[(r * length) + c] = ((*data_pointer)->data)[(start_row * full_length ) + start_column];
      }
    }
  //Encapsulate as an image structure so we can point to it
    img im;
    im.data = cropped;
    im.rows = length;
    im.cols = width;
    im.colors = colors;
    //free((*data_pointer)->data);
    **data_pointer = im;
}
  


unsigned char saturation(double intial, int change){
  unsigned char final;
  if((int) intial + change > 255){ //prevent overflow
    final = 255;
    return final;
  } else {
    if((int) intial + change < 0){//prevent underflow
       final = 0;
       return final;
    } else {//add the change
       int temp = (int) intial + change;
       final = (unsigned char) temp;
       return final;
     }
  }
}

//the function to make the image greyscale, takes in a pointer to the image and
//changes all colors in a given pixel to equal the intensity
void grayscale(img **data_pointer){
 
   pix *grayscale = malloc(sizeof(pix) * (*data_pointer)->cols * (*data_pointer)->rows);
   
   //the variable for the intensity of all pixels
   unsigned char intensity;
  	
   //loop through data array to calculate and set the intensity of each pixel
   for(int r = 0; r < (*data_pointer)->rows; r++) {
	   for (int c = 0; c < (*data_pointer)->cols; c++){
		   
		   //set value of intensity from equation:  intensity = 0.30*red + 0.59*green + 0.11*blue;
		   intensity = 0.30*((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).red) 
			   + 0.59*((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).green) 
			   + 0.11*((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).blue);
		   
		   //set the values of red, green, blue of current pixel to intensity
		   ((grayscale)[(r * (*data_pointer)->cols) + c]).red = intensity;
		   ((grayscale)[(r * (*data_pointer)->cols) + c]).green = intensity;
		   ((grayscale)[(r * (*data_pointer)->cols) + c]).blue = intensity;

		
   }
}
   //Encapsulate as an image structure so we can point to it
   img im;
   im.data = grayscale;
   im.rows = (*data_pointer)->rows;
   im.cols = (*data_pointer)->cols;
   im.colors = (*data_pointer)->colors;
   free((*data_pointer)->data);
   **data_pointer = im;

}

void brightness(img **data_pointer, int change){
  //Initialize values to use later  
  int columns = (*data_pointer)->cols;
  int rows = (*data_pointer)->rows;
  pix *brightened = malloc(sizeof(pix) * columns * rows);
  
  
  for (int r = 0; r < rows; r++){
    for (int c = 0; c < columns; c++){
      //Take each rgb value and cast as a double to use for saturation
      double red = (double) (((*data_pointer)->data)[(r * columns) + c].red);
      double green = (double) (((*data_pointer)->data)[(r * columns) + c].green);
      double blue = (double) (((*data_pointer)->data)[(r * columns ) + c].blue);

      //Add the change, accounting for saturation
      unsigned char re = saturation(red, change);
      unsigned char g = saturation(green, change);
      unsigned char b = saturation(blue, change);

      //Store new values into our allocated location
      brightened[(r * columns) + c].red = re;
      brightened[(r * columns) +c].green = g;
      brightened[(r *columns) +c].blue = b;
    }
  }
  //Encapsulate as an image structure so we can point to it
  img im;
  im.data = brightened;
  im.cols = columns;
  im.rows = rows;
  im.colors = (*data_pointer)->colors;
  free((*data_pointer)->data);
  **data_pointer = im; 
}

//the funciton to swap the components of the pixel (green to red, blue to green, and red to blue)
void swap(img **data_pointer){

	//allocate the memory required to hold the new data
	pix *swap = malloc(sizeof(pix) * (*data_pointer)->cols * (*data_pointer)->rows);
	unsigned char temp;

	//reassign the value of green to red, blue to green, and red to blue
	for (int r = 0; r < (*data_pointer)->rows; r++){
		for (int c = 0; c < (*data_pointer)->cols; c++){
			temp = (((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).red;
			((swap)[(r * (*data_pointer)->cols) + c]).red = (((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).green;
			((swap)[(r * (*data_pointer)->cols) + c]).green = (((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).blue;
			((swap)[(r * (*data_pointer)->cols) + c]).blue = temp;
		}
	}

	//Encapsulate as an image structure so we can point to it
   	img im;
	im.data = swap;
   	im.rows = (*data_pointer)->rows;
   	im.cols = (*data_pointer)->cols;
   	im.colors = (*data_pointer)->colors;
	free((*data_pointer)->data);
	**data_pointer = im;
}	


void pieces_to_pointer(img **data_pointer, pix *structure, int columns, int rows, int colors){
  img im;
  im.data = structure;
  im.cols = columns;
  im.rows = rows;
  im.colors = colors;
  **data_pointer = im;
}


int transform_sigma(double sigma){
  int length = sigma * 10;
  if(length % 2 == 0){
    length += 1;
  }
  return length;
}
    
double sq(double value){
   return value * value;
}

unsigned char sqr(unsigned char value){
	return value * value;
}

void populate_matrix(int transformed_sigma, double weights[][transformed_sigma], double sigma){
  for(int i = 0; i < transformed_sigma; i++){
    for(int j = 0; j < transformed_sigma; j++){
      int middle = (transformed_sigma / 2) + 1;
      int dx  = middle - i - 1;
      int dy = middle - j - 1;
      weights[i][j] = ((1.0 / (2.0 * pi * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma))));
    }
  }
}

signed int set_l_t(int pix_pos, double radius){
  if (pix_pos - radius < 0){
    return (pix_pos - radius) * -1;
  } else {
    return 0;
  }
}

signed int set_r_b(int pix_pos, double radius, int max){
  if(radius + pix_pos > max){
    return max - pix_pos -1 + radius;
  } else {
    return (int) radius * 2;
  }
}

double matrix_into_char(int size, double matrix[size][size], int top, int bot, int right, int left){
  double storage = 0;
  for(int i = left; i < right; i++){
    for(int j = top; j < bot; j++){
      storage += matrix[i][j];
    }
  }
  return storage;
}

      

//the function to blur the image by some input amount
void blur(int transformed_sigma, double sigma, img **data_pointer){
  //Initialize values to use later
  int columns = (*data_pointer)->cols;
  int rows = (*data_pointer)->rows;
  int colors = (*data_pointer)->colors;
  pix *blurred = malloc(sizeof(pix) * columns * rows);

  for(int r = 0; r < rows; r++){
    for(int c = 0; c < columns; c++){

      //Create a matrix of weights
      double weights[transformed_sigma][transformed_sigma];
      populate_matrix(transformed_sigma, weights, sigma);

      //Initialize arrays to hold weighted pixel values
      double red[transformed_sigma][transformed_sigma];
      double green[transformed_sigma][transformed_sigma];
      double blue[transformed_sigma][transformed_sigma];
      
      int radius = transformed_sigma / 2;
      

      //int adjusted_r = r - radius;
      //int adjusted_c = c - radius;

      //Loop through matrix, making sure not to go out of bounds
      /*for(int i = -radius; i < radius + 1; i++){
	for (int j = -radius; j < radius + 1; j++){
	  if(!(adjusted_c  < 0 || adjusted_c > columns || adjusted_r < 0 || adjusted_r - 5  > rows)){
	    red[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].red);
	    green[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].green);
	    blue[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].blue);
	  } adjusted_r++;
        } adjusted_c++;
      }*/

      for(int i = -radius; i < radius + 1; i++){
	for (int j = -radius; j < radius + 1; j++){
	  if(!(c + j < 0 || c + j > columns || r + i < 0 || r + i > rows)){
	    red[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((r + i) * columns) + (c + j)].red);
	    green[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((r + i) * columns) + (c + j)].green);
	    blue[i + radius][j + radius] = weights[i + radius][j + radius] * (((*data_pointer)->data)[((r + i) * columns) + (c + j)].blue);
	  }
        }
      }

      /**
      //Added to see if we could fix valgrind but it didnt work
      for (int i = 0; i < transformed_sigma; i++){
	  for(int j = 0; j < transformed_sigma; j++){
	    if(!(adjusted_c < 0 || adjusted_c - 10 > columns || adjusted_r < 0 || adjusted_r > rows)){
	    red[i][j] = weights[i][j] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].red);
	    green[i][j] = weights[i][j] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].green);
	    blue[i][j] = weights[i][j] * (((*data_pointer)->data)[((adjusted_r) * columns) + (adjusted_c)].blue);
	    }
	    adjusted_c++;
	  }
	  adjusted_r++;
      }
      */ 
	    
		

      int top = set_l_t(r, radius);
      int left = set_l_t(c, radius);
      int bot = set_r_b(r, radius, rows);
      int right = set_r_b(c, radius, columns);

      //Sum the values in our weight matrix
      double new_weights[transformed_sigma][transformed_sigma];
      populate_matrix(transformed_sigma, new_weights, sigma);
      double total_weight = matrix_into_char(transformed_sigma, weights, top, bot, right, left);

      //Set each value to the correct unsigned char
      unsigned char final_red = (unsigned char) (matrix_into_char(transformed_sigma, red, top, bot, right, left))/ total_weight;
      unsigned char final_green = (unsigned char) (matrix_into_char(transformed_sigma, green, top, bot, right, left))/ total_weight;
      unsigned char final_blue = (unsigned char)  (matrix_into_char(transformed_sigma, blue, top, bot, right, left))/ total_weight;
      
      pix color = {final_red, final_green, final_blue};
      blurred[(r * columns) + c] = color;
    }
  }
  //Encapsulate as an image
  img im;
  im.data = blurred;
  im.rows = rows;
  im.cols = columns;
  im.colors = colors;
  free((*data_pointer)->data);
  **data_pointer = im;

}

//the function to sharpen the image
void sharpen(img **data_pointer, double sigma, double amount){
  //Intialize values to use later
   int columns = (*data_pointer)->cols;
   int rows = (*data_pointer)->rows;
   pix *storage = malloc(sizeof(pix) * columns * rows); 
   
   //Copy over our original values to use later
   pix temp[rows * columns];
   for(int r = 0; r < rows; r++){
     for(int c = 0; c < columns; c++){
       temp[(r * columns) + c] = ((*data_pointer)->data)[(r * columns) + c];
     }
   }

   //Blur the image
   int changed_sigma = transform_sigma(sigma);
   blur(changed_sigma, sigma, data_pointer);

   //Sharpen by bringing blurred and original image together
   for(int r = 0; r < rows; r++){
     for(int c = 0; c < columns; c++){
       unsigned char redred = (((*data_pointer)->data)[(r * columns) + c].red);
       storage[(r * columns) + c].red = saturation(((temp[(r * columns) + c].red) - redred) * amount, temp[(r * columns) + c].red);

       unsigned char greengreen = (((*data_pointer)->data)[(r * columns) + c].green);
       storage[(r * columns) + c].green = saturation(((temp[(r * columns) + c].green) - greengreen)* amount, temp[(r * columns) + c].green);
 
       unsigned char blueblue = (((*data_pointer)->data)[(r * columns) + c].blue);
       storage[(r * columns) + c].blue = saturation(((temp[(r * columns) + c].blue) - blueblue)* amount,  temp[(r * columns) + c].blue);

     }
   }
   img im;
   im.data = storage;
   im.rows = rows;
   im.cols = columns;
   im.colors = (*data_pointer)->colors;
   free((*data_pointer)->data);
   **data_pointer = im;
  
}

//contrast sub-function to convert unsigned chars in [0, 255] to doubles in [-127.5, 127.5]
double convert_double(unsigned char u){
	
	double new;
	double uc_new = (double) u;
	
	//range will be [-127.5, 127.5]
	if (u >= 127.5){
		new = (uc_new - 127.5);
	}
	else if(u < 127.5){
		new = (uc_new - 127.5);
	}
	return new;
}

//contrast sub-function to convert doubles in ([-127.5, 127.5] * adjustment factor) back to unsigned chars in [0, 255]
unsigned char convert_back(double dub){
	double new;
	
	//get back into the range of [0, 255]
	if (dub > 0){
		new = (dub + 127.5);
		if (new > 255) {
			new = 255;
		}
	}
	else if (dub < 0){
	  new = (dub +127.5);
	  if ( new < 0){
		new = 0;
	  }
	}
	return (unsigned char) new;
}

//the function to apply an input amount of contrast to the image
void contrast(img **data_pointer, double adjustment){
	
	//allocating the memory to hold the new data of the pix colors
	pix *contrast = malloc(sizeof(pix) * (*data_pointer)->rows * (*data_pointer)->cols);
	
	//allocating the memory to hold the new data of double_pix colors
	dpix *temp = malloc(sizeof(dpix) * (*data_pointer)->rows * (*data_pointer)->cols);

	//loop to change all unsigned chars to double of new interval [-127.5, 127.5]
	for (int r = 0; r < (*data_pointer)->rows; r++){
		for (int c = 0; c < (*data_pointer)->cols; c++){
			//call convert_double on the values of red, green, and blue of each pixel 
			((temp)[(r * (*data_pointer)->cols) + c]).red = convert_double((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).red);
			//printf("%u\t", (((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).red);
			//printf("%f\n", convert_double((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).red));
			((temp)[(r * (*data_pointer)->cols) + c]).green = convert_double((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).green);
			((temp)[(r * (*data_pointer)->cols) + c]).blue = convert_double((((*data_pointer)->data)[(r * (*data_pointer)->cols) + c]).blue);
		}
	}
	//multiply each value of red, green, adn blue in the new array by the adjustment factor
	for (int r2 = 0; r2 < (*data_pointer)->rows; r2++){
                for (int c2 = 0; c2 < (*data_pointer)->cols; c2++){
			((temp)[(r2 * (*data_pointer)->cols) + c2]).red *= adjustment;
			((temp)[(r2 * (*data_pointer)->cols) + c2]).green *= adjustment;
			((temp)[(r2 * (*data_pointer)->cols) + c2]).blue *= adjustment;
		}
	}

	//reseting the color value of each pixel to the new modified (contrasted) value
	for (int r3 = 0; r3 < (*data_pointer)->rows; r3++){
		for (int c3 = 0; c3 < (*data_pointer)->cols; c3++){
			((contrast)[(r3 * (*data_pointer)->cols) + c3]).red = convert_back(((temp)[(r3 * (*data_pointer)->cols) + c3]).red);
			((contrast)[(r3 * (*data_pointer)->cols) + c3]).green = convert_back(((temp)[(r3 * (*data_pointer)->cols) + c3]).green);
			((contrast)[(r3 * (*data_pointer)->cols) + c3]).blue = convert_back(((temp)[(r3 * (*data_pointer)->cols) + c3]).blue);
		}
	}
	int columns = (*data_pointer)->cols;
	int rows = (*data_pointer)->rows;
	int colors = (*data_pointer)->colors;
	img im;
	im.data = contrast;
	im.cols = columns;
	im.rows = rows;
	im.colors = colors;
        free(temp);
	free((*data_pointer)->data);
	**data_pointer = im;
	
}

//function for computing the x-component of the gradient of the intensity
unsigned char gradIx(img **data_pointer, int row, int col){
	unsigned char GradIx;

	//formula for GradIx = [I(x+1, y) - I(x-1,y) / 2] where x is row-value, y is col-value
	GradIx = (((((*data_pointer)->data)[((row) * (*data_pointer)->cols) + (col + 1)]).red
		- (((*data_pointer)->data)[((row) * (*data_pointer)->cols) + (col - 1)]).red) / 2);
	
	return GradIx;
}

//function for computing the y-component of the gradient of the intensity
unsigned char gradIy(img **data_pointer, int row, int col){ //******check int values is okay
	unsigned char GradIy;

        //formula for GradIy = [I(x, y+1) - I(x,y-1) / 2] where x is row-value, y is col-value
        GradIy = (((((*data_pointer)->data)[((row + 1) * (*data_pointer)->cols) + (col)]).red
                - (((*data_pointer)->data)[((row - 1) * (*data_pointer)->cols) + (col)]).red) / 2);

        return GradIy;
}

//function for computing the magnitude of the gradient of intensity
unsigned char gradMag(unsigned char gradIx, unsigned char gradIy){
	return (unsigned char) pow((sqr(gradIx) + sqr(gradIy)), 0.5);
}


//function for filtering the image with edge detection
void edge_detection(img **data_pointer, unsigned char threshold, int transformed_sigma, double sigma){
	
	//convert to grayscale and blur
 	grayscale(data_pointer);
	blur(transformed_sigma, sigma, data_pointer);

	//allocate the space for the intesity grad for each pixel not on the boundary of the photo
	unsigned char *grad = malloc(sizeof(pix) * ((*data_pointer)->rows - 1) * ((*data_pointer)->cols - 1)); 

	for (int r = 1; r < ((*data_pointer)->rows - 1); r++){
	       for (int c = 1; c < ((*data_pointer)->cols - 1); c++){
		       unsigned char GradIx = gradIx(data_pointer, r, c);
		       unsigned char GradIy = gradIy(data_pointer, r, c);
		       unsigned char GradMag = gradMag(GradIx, GradIy);
		       (grad)[((r - 1) * (*data_pointer)->cols - 1) + (c - 1)] = GradMag;
	       }
	}

	for (int r2 = 1; r2 < ((*data_pointer)->rows - 1); r2++){
               for (int c2 = 1; c2 < ((*data_pointer)->cols - 1); c2++){
		       if((grad)[((r2 - 1) * (*data_pointer)->cols - 1) + (c2 - 1)] > threshold){
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).red = 0;
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).green = 0;
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).blue = 0;
		       }
		       else {
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).red = 255;
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).green = 255;
			       (((*data_pointer)->data)[(r2 * (*data_pointer)->cols) + c2]).blue = 255;
	       	       }
	       }

	}

	//free(grad);
	
}
